<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = [];
        for ($i = 1; $i<100; $i++){
            array_push($posts,['title'=>'test article '.$i,
                       'body' => 'this is test body and you can see this article number like this showing more than needs you could buy skateboard and start kit from maxfind '. $i,
                       'slug' =>'test-'.'article-'.$i]);
        }
        /*$posts = [
            [
                'title'=>'test ',
                'body'=>'user1@test.com',
            ],
            [
                'name' => '사용자2',
                'email'=>'user2@test.com',
                'password'=>bcrypt('secret'),
            ]
        ];*/

        foreach ($posts as $p) {
            App\Post::create($p);
        }
    }
}
