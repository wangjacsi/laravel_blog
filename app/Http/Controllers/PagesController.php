<?php
namespace App\Http\Controllers;
use App\Post;

class PagesController extends Controller {
    public function getIndex(){
        $posts = Post::orderBy('created_at', 'desc')->limit(4)->get();
        return view('pages.welcome')->withPosts($posts);
    }

    public function getAbout(){

        $first = 'Sean';
        $last = 'Choi';

        $fullname = $first . ' '. $last;
        $email = 'wangjacsi@gmail.com';

        //return view('pages.about')->with('fullname', $fullname);
        //return view('pages.about')->withFullname($fullname)->withEmail($email);
        $data = array('fullname'=>$fullname, 'email'=>$email);
        return view('pages.about')->withData($data);
    }

    public function getContact(){
        return view('pages.contact');
    }



}
